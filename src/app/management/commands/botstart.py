from django.core.management.base import BaseCommand

from app.internal.bot import bot


class Command(BaseCommand):
    help = "Run telegram bot"

    def handle(self, *args, **options):
        bot.start_polling()
        bot.idle()
