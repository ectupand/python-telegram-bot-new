from telegram.ext import CommandHandler, Updater

from app.internal.transport.bot.handlers import me, set_phone, start
from config.settings import TELEGRAM_TOKEN

bot = Updater(token=TELEGRAM_TOKEN)
bot.dispatcher.add_handler(CommandHandler("start", start))
bot.dispatcher.add_handler(CommandHandler("set_phone", set_phone))
bot.dispatcher.add_handler(CommandHandler("me", me))
