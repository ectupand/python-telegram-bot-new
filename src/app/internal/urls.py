from django.urls import path

from app.internal.transport.rest.handlers import me

urlpatterns = [path("me/", me, name="me")]
