from django.core import signing
from django.core.signing import BadSignature
from django.shortcuts import render
from django.utils.datastructures import MultiValueDictKeyError

from app.internal.models.person import Person


def me(request):
    signer = signing.TimestampSigner()

    try:
        encoded_id = request.GET["tg_id"]
        tg_id = signer.unsign_object(encoded_id)
        person = Person.objects.get(telegram_id=tg_id)
    except BadSignature:
        return render(request, "404.html")
    except MultiValueDictKeyError:
        return render(request, "404.html")
    phone = person.phone_number

    context = {"person": person, "phone": phone}
    return render(request, "me.html", context)
