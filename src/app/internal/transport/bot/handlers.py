import re

from telegram import Update
from telegram.ext import CallbackContext

from app.internal.models.person import Person
from app.internal.services.user_service import add_phone, create_person, return_info
from app.internal.transport.bot.constants import (
    ALREADY_REGISTERED,
    INVALID_NUMBER,
    NAME_SIGNUP_SUCCESS,
    NUMBER_IS_NULL,
    NUMBER_SIGNUP_SUCCESS,
    OVERALL_INFO,
)


def start(update: Update, context: CallbackContext) -> None:
    user_exists = Person.objects.filter(telegram_id=update.effective_user.id)
    if user_exists:
        update.message.reply_text(ALREADY_REGISTERED)
        return
    create_person(telegram_id=update.effective_user.id, username=update.effective_user.username)

    update.message.reply_text(NAME_SIGNUP_SUCCESS.format(update.effective_user.username))


def set_phone(update: Update, context: CallbackContext) -> None:
    if len(context.args) != 1:
        update.message.reply_text(NUMBER_IS_NULL)
        return

    phone_number = context.args[0]
    if not _valid_number(phone_number):
        update.message.reply_text(INVALID_NUMBER)
        return
    add_phone(telegram_id=update.effective_user.id, phone=phone_number)
    update.message.reply_text(NUMBER_SIGNUP_SUCCESS.format(phone_number))


def _valid_number(phone_number):
    pattern = re.compile(r"^\+?1?\d{9,16}$", re.IGNORECASE)
    return pattern.match(phone_number)


def me(update: Update, context: CallbackContext) -> None:
    username, phone = return_info(telegram_id=update.effective_user.id)
    if not phone:
        return
    update.message.reply_text(OVERALL_INFO.format(username, phone))
