from app.internal.models.person import Person


def create_person(username, telegram_id):
    Person.objects.create(username=username, telegram_id=telegram_id)


def add_phone(telegram_id, phone):
    person = Person.objects.get(telegram_id=telegram_id)
    person.phone_number = phone
    person.save()


def return_info(telegram_id):
    person = Person.objects.get(telegram_id=telegram_id)
    if person.phone_number:
        return person.username, person.phone_number
    return person.username, False
