from django.contrib import admin

from app.internal.models.person import Person


@admin.register(Person)
class PersonAdmin(admin.ModelAdmin):
    fields = ["username", "phone_number", "telegram_id"]
    empty_value_display = "-пусто-"
    list_display = ("username", "phone_number", "date_joined")
    list_filter = ("date_joined",)
