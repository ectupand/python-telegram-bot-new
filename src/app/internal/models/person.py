from django.core.validators import RegexValidator
from django.db import models


class Person(models.Model):
    username = models.CharField(max_length=255, unique=True)
    telegram_id = models.IntegerField(primary_key=True, unique=True)
    date_joined = models.DateTimeField(auto_now_add=True)
    phone_number = models.CharField(max_length=16, validators=[RegexValidator(regex=r"^\+?1?\d{9,16}$")], blank=True)

    def __str__(self):
        return self.username

    class Meta:
        verbose_name = "Person"
        verbose_name_plural = "People"
